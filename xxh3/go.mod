module catinello.eu/x/xxh3

go 1.18

require (
	catinello.eu/x/avo v0.0.0-20220810144547-9c1d363ba56c
	catinello.eu/x/cpuid v0.0.0-20220810144547-9c1d363ba56c
)

require (
	golang.org/x/mod v0.6.0-dev.0.20220419223038-86c51ed26bb4 // indirect
	golang.org/x/sys v0.0.0-20220804214406-8e32c043e418 // indirect
	golang.org/x/tools v0.1.12 // indirect
)
