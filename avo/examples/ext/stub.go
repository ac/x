// Package ext demonstrates how to interact with external types.
package ext

import "catinello.eu/x/avo/examples/ext/ext"

// StructFieldB returns field B.
func StructFieldB(e ext.Struct) byte
