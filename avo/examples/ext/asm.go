//go:build ignore
// +build ignore

package main

import . "catinello.eu/x/avo/build"

func main() {
	Package("catinello.eu/x/avo/examples/ext")
	Implement("StructFieldB")
	b := Load(Param("e").Field("B"), GP8())
	Store(b, ReturnIndex(0))
	RET()
	Generate()
}
