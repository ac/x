package load_test

import (
	"testing"

	"catinello.eu/x/avo/internal/gen"
	"catinello.eu/x/avo/internal/inst"
	"catinello.eu/x/avo/internal/load"
	"catinello.eu/x/avo/internal/test"
	"catinello.eu/x/avo/printer"
)

func Load(t *testing.T) []inst.Instruction {
	t.Helper()
	l := load.NewLoaderFromDataDir("testdata")
	is, err := l.Load()
	if err != nil {
		t.Fatal(err)
	}
	return is
}

func TestAssembles(t *testing.T) {
	is := Load(t)
	g := gen.NewAsmTest(printer.NewArgvConfig())
	b, err := g.Generate(is)
	if err != nil {
		t.Fatal(err)
	}
	test.Assembles(t, b)
}
