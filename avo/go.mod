module catinello.eu/x/avo

go 1.18

require (
	golang.org/x/arch v0.0.0-20220722155209-00200b7164a7
	golang.org/x/sys v0.0.0-20220804214406-8e32c043e418
	golang.org/x/tools v0.1.12
)

require golang.org/x/mod v0.6.0-dev.0.20220419223038-86c51ed26bb4 // indirect
