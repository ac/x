//go:build ignore
// +build ignore

package main

import (
	. "catinello.eu/x/avo/build"
	. "catinello.eu/x/avo/operand"
	. "catinello.eu/x/avo/reg"
)

func main() {
	TEXT("Issue65", NOSPLIT, "func()")
	VINSERTI128(Imm(1), Y0.AsX(), Y1, Y2)
	RET()
	Generate()
}
