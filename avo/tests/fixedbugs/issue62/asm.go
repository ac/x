//go:build ignore
// +build ignore

package main

import . "catinello.eu/x/avo/build"

func main() {
	Package("catinello.eu/x/avo/tests/fixedbugs/issue62")
	Implement("private")
	RET()
	Generate()
}
