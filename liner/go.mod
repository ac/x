module catinello.eu/x/liner

go 1.21.5

require github.com/mattn/go-runewidth v0.0.15

require github.com/rivo/uniseg v0.4.4 // indirect
