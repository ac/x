module catinello.eu/x/oto

go 1.18

require (
	golang.org/x/mobile v0.0.0-20220722155234-aaac322e2105
	golang.org/x/sys v0.0.0-20220804214406-8e32c043e418
)

require (
	golang.org/x/exp v0.0.0-20190731235908-ec7cb31e5a56 // indirect
	golang.org/x/image v0.0.0-20190802002840-cff245a6509b // indirect
)
